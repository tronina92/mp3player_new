package utils;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileUtils {

    // получить имя файла без расширения
    public static String getFileNameWithoutExtension(String fileName){

        File file = new File(fileName);
        int index = file.getName().lastIndexOf('.');
        if (index > 0 && index <=file.getName().length() -2){
            return file.getName().substring(0, index);
        }
        return "noname";
    }

    // получить расширение файла
    public static String getFileExtension(File f){
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');
        if (i > 0 && i <=s.length() -1){
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }

    // удалить текущий файл-фильтр и установить новый переданный
    public static void addFileFilter (JFileChooser jFileChooser, FileFilter fileFilter) {
        jFileChooser.removeChoosableFileFilter(jFileChooser.getFileFilter());
        jFileChooser.setFileFilter((javax.swing.filechooser.FileFilter) fileFilter);
        jFileChooser.setSelectedFile(new File("")); // удалить последнее имя открываемого/сохраняемого файла
    }

    public static void serialize (Object o, String fileName) {
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.flush();
            oos.close();
            fos.close();
        } catch (IOException e) {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE,null,e);
        }
    }

    public static Object deserialize (String fileName) {
        try {
            FileInputStream fis = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object ts = (Object) ois.readObject();
            fis.close();
            return ts;
        } catch (IOException | ClassNotFoundException e) {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE,null,e);
        }
        return null;
    }
}
