package utils;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestSong {

    public static void main(String[] args) {

        TestSong testSong = new TestSong();
        ArrayList<Song> playlist = new ArrayList<>();

        for (int i=0; i<5; i++) {
            playlist.add(new Song("song " + i, "path "+i));
            System.out.println(playlist.get(i));
        }

        testSong.searchSong("song", playlist);

    }

    public void searchSong(String regex, ArrayList<Song> playlist) {

        for (Song song:playlist) {
            if (song.getName().contains(regex)) System.out.println(song.getName());
        }
    }
}
