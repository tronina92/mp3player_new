package utils;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class PlayListFileFilter extends FileFilter {

    private String fileExtension;
    private String fileDescription;

    public PlayListFileFilter(String fileDescription, String fileExtension) {
        this.fileDescription = fileDescription;
        this.fileExtension = fileExtension;
    }

    @Override
    public boolean accept(File f) {
        return f.isDirectory()||f.getAbsolutePath().endsWith(fileExtension);
    }

    @Override
    public String getDescription() {
        return fileDescription + " (*." + fileExtension + ")";
    }
}
