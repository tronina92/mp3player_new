package utils;

import javax.swing.filechooser.FileFilter;
import java.io.File;

// фильтр для возможности выбора файлов только с раширением mp3 для компонента FileChooser

public class MP3PlayerFileFilter extends FileFilter {

    private String fileExtension;
    private String fileDescription;

    public MP3PlayerFileFilter(String fileDescription, String fileExtension) {
        this.fileDescription = fileDescription;
        this.fileExtension = fileExtension;
    }

    @Override
    public boolean accept(File f) {
        return f.isDirectory()||f.getAbsolutePath().endsWith(fileExtension);
    }

    @Override
    public String getDescription() {
        return fileDescription + " (*." + fileExtension + ")";
    }
}
