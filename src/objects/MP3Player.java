package objects;

import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;
import javazoom.jlgui.basicplayer.BasicPlayerListener;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MP3Player {

    public MP3Player (BasicPlayerListener listener) {
        player.addBasicPlayerListener(listener);
    }

    private BasicPlayer player = new BasicPlayer();

    private String currentFileName;
    private double currentVolumeValue;

    public void play(String fileName) {

        try {
            if (currentFileName != null && currentFileName.equals(fileName) && player.getStatus() == BasicPlayer.PAUSED) {
                player.resume();
                return;
            }

            currentFileName = fileName;
            player.open(new File(fileName));
            player.play();
            player.setGain(currentVolumeValue);

        } catch (BasicPlayerException e) {
            Logger.getLogger(MP3Player.class.getName()).log(Level.SEVERE,null,e);
        }
    }

    public void stop() {
        try {
            player.stop();
        } catch (BasicPlayerException e) {
            Logger.getLogger(MP3Player.class.getName()).log(Level.SEVERE,null,e);
        }
    }

    public void pause(){
        try {
            player.pause();
        } catch (BasicPlayerException e) {
            Logger.getLogger(MP3Player.class.getName()).log(Level.SEVERE,null,e);
        }
    }

    public void setVolume(int currentVolumeValue, int maximumValue) {
        try {
            this.currentVolumeValue = currentVolumeValue;

            if (currentVolumeValue==0) {
                player.setGain(0);
            } else {
                player.setGain(calcVolume(currentVolumeValue, maximumValue));
            }
        } catch (BasicPlayerException e) {
            Logger.getLogger(MP3Player.class.getName()).log(Level.SEVERE,null,e);
        }
    }

    private double calcVolume(int currentValue, int maximumValue) {
        currentVolumeValue = (double) currentValue/(double) maximumValue;
        return currentVolumeValue;
    }

    public void jump (long bytes) {
        try {
            player.seek(bytes);
            player.setGain(currentVolumeValue);
        } catch (BasicPlayerException e) {
            Logger.getLogger(MP3Player.class.getName()).log(Level.SEVERE,null,e);
        }
    }
}
