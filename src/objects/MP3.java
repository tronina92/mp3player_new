package objects;

import utils.FileUtils;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

public class MP3 implements Serializable {

    private String name;
    private String path;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public MP3(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public boolean compareMP3s(MP3 mp3two){
        return this.getName().equals(mp3two.getName()) && this.getPath().equals(mp3two.getPath());
    }

    // для корректного отображения объекта MP3 при добавлении в плейлист
    @Override
    public String toString() {
        return FileUtils.getFileNameWithoutExtension(name);
    }
}
