package GUI;

import javazoom.jlgui.basicplayer.BasicController;
import javazoom.jlgui.basicplayer.BasicPlayerEvent;
import javazoom.jlgui.basicplayer.BasicPlayerListener;
import objects.MP3;
import objects.MP3Player;
import utils.FileUtils;
import utils.MP3PlayerFileFilter;
import utils.PlayListFileFilter;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import static java.awt.event.KeyEvent.VK_ENTER;

public class MP3PlayerGUI extends JFrame implements BasicPlayerListener {

    private static final String MP3_FILE_EXTENSION = "mp3";
    private static final String MP3_FILE_DESCRIPTION = "файлы mp3";
    private static final String PLAYLIST_FILE_EXTENSION = "pls";
    private static final String PLAYLIST_FILE_DESCRIPTION = "файлы плейлиста";
    private static final String EMPTY_STRING = "";

    private JPanel mainPanel;
    private JPanel searchPanel;
    private JPanel centralPanel;
    private JTextField searchSongTextField;
    private JButton searchButton;
    private JButton addButton;
    private JButton prevButton;
    private JButton nextButton;
    private JButton removeButton;
    private JScrollPane scroll;
    private JList plsList;
    private JLabel songNameLabel;
    private JSlider progressSlider;
    private JSlider volumeSlider;
    private JButton prevSongButton;
    private JButton playButton;
    private JButton resumeButton;
    private JButton stopButton;
    private JButton nextSongButton;
    private JToggleButton btnMute;

    //<editor-fold desc="компоненты MenuBar">
    private JMenuBar menuBar = new JMenuBar();
    private JMenu fileMenu = new JMenu("File");
    private JMenu serviceMenu = new JMenu("Settings");
    private JMenuItem openItem = new JMenuItem("Open...");
    private JMenuItem saveItem = new JMenuItem("Save");
    private JMenuItem exitItem = new JMenuItem("Exit");
    private JMenu changeSkinMenu = new JMenu("Change skin");
    private JMenuItem menuSkin1 = new JMenuItem("Skin 1");
    private JMenuItem menuSkin2 = new JMenuItem("Skin 2");
    //</editor-fold>

    //<editor-fold desc="компоненты PopupMenu">
    private JPopupMenu jpm = new JPopupMenu();
    private JMenuItem addSongMenuItem = new JMenuItem("Добавить песню");
    private JMenuItem deleteSongMenuItem = new JMenuItem("Удалить песню");
    private JMenuItem openPlsMenuItem = new JMenuItem("Открыть плейлист");
    private JMenuItem clearPlsMenuItem = new JMenuItem("Очистить плейлист");
    private JMenuItem playSongMenuItem = new JMenuItem("Play");
    private JMenuItem stopSongMenuItem = new JMenuItem("Stop");
    private JMenuItem pauseSongMenuItem = new JMenuItem("Pause");
    //</editor-fold>

    final JFileChooser fc = new JFileChooser();
    private DefaultListModel mp3ListModel = new DefaultListModel();
    private FileFilter mp3FileFilter = new MP3PlayerFileFilter(MP3_FILE_DESCRIPTION, MP3_FILE_EXTENSION);
    private FileFilter playListFileFilter = new PlayListFileFilter(PLAYLIST_FILE_DESCRIPTION, PLAYLIST_FILE_EXTENSION);

    private MP3Player player = new MP3Player(this);

    int currentVolumeValue;

    //<editor-fold desc="переменные для прокрутки песни">
    private long secondsAmount; // сколько секунд прошло с начала проигрывания
    private long duration; // длительность песни в секундах
    private int bytesLen; // размер песни в байтах
    private double posValue = 0.0; // позиция для прокрутки
    private boolean movingFromJump = false;// передвигается ли ползунок песни от перетаскивания (или от проигрывания) - используется во время перемотки
    private boolean moveAutomatic = false;// во время проигрывании песни ползунок передвигается, moveAutomatic = true
    //</editor-fold>

    //<editor-fold desc="методы из BasicPlayerListener">
    @Override
    public void opened(Object o, Map map) {

        // определить длину песни в секундах и размер файла в байтах
        duration = (long) Math.round((((Long) map.get("duration")).longValue())/1000000);
        bytesLen = (int) Math.round(((Integer) map.get("mp3.length.bytes")).intValue());

        // если есть mp3 тег для имени - берем его, если нет - вытаскиваем название из имени файла
        String songName = map.get("title") != null ? map.get("title").toString() : FileUtils.getFileNameWithoutExtension(new File(o.toString()).getName());

        // если длинное название - укоротить его
        if (songName.length() > 30) {
            songName = songName.substring(0,30) + "...";
        }
    } //при запуске файла

    @Override
    public void progress(int bytesread, long microseconds, byte[] pcmdata, Map properties) {

        float progress = -1.0f;

        if ((bytesread>0) && (duration>0)) {
            progress = bytesread * 1.0f / bytesLen * 1.0f; //какая часть песни проиграна
        }

        secondsAmount = (long) (duration * progress); //сколько секунд прошло

        if (duration != 0) {
            if (movingFromJump == false) {
                progressSlider.setValue(((int) Math.round(secondsAmount * 1000 / duration) ));
            }
        }

    } //управление ползунком прогресса песни

    @Override
    public void stateUpdated(BasicPlayerEvent basicPlayerEvent) {
        int state = basicPlayerEvent.getCode();

        if (state == BasicPlayerEvent.PLAYING) {
            movingFromJump = false;
        } else if (state == BasicPlayerEvent.SEEKING) {
            movingFromJump = true;
        } else if (state == BasicPlayerEvent.EOM) {
            if (selectNextSong()) {
                playFile();
            }
        }
    } //вызывается каждый раз, когда измененяется состояние песни

    @Override
    public void setController(BasicController basicController) {

    }
    //</editor-fold>

    private void playFile() {
        int [] indexPlayList = plsList.getSelectedIndices();
        if (indexPlayList.length > 0) {
            MP3 mp3 = (MP3) mp3ListModel.getElementAt(indexPlayList[0]);
            player.play(mp3.getPath());
            player.setVolume(volumeSlider.getValue(), volumeSlider.getMaximum());
            songNameLabel.setText("Текущая песня: " + mp3.getName());
        }
    }

    private boolean selectNextSong() {
        int nextIndex = plsList.getSelectedIndex() + 1;
        if (nextIndex <= plsList.getModel().getSize() - 1) {
            plsList.setSelectedIndex(nextIndex);
            return true;
        }
        return false;
    }

    private boolean selectPrevSong(){
        int nextIndex = plsList.getSelectedIndex() - 1;
        if (nextIndex >= 0) {
            plsList.setSelectedIndex(nextIndex);
            return true;
        }
        return false;
    }

    private void processSeek(double bytes) {
        try {
            long skipBytes = (long) Math.round(((Integer) bytesLen).intValue() * bytes);
            player.jump(skipBytes);
        } catch (Exception e) {
            e.printStackTrace();
            movingFromJump = false;
        }
    }

    class AddSongActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            FileUtils.addFileFilter(MP3PlayerGUI.this.fc, MP3PlayerGUI.this.mp3FileFilter);
            int result = MP3PlayerGUI.this.fc.showOpenDialog(MP3PlayerGUI.this);//result хранит выбран файл или нет

            if (result==JFileChooser.APPROVE_OPTION) {
                File []selectedFiles = MP3PlayerGUI.this.fc.getSelectedFiles();

                for(File file:selectedFiles) {
                    int count=0;
                    MP3 mp3 = new MP3(file.getName(), file.getPath());
                    for (int i=0; i<MP3PlayerGUI.this.mp3ListModel.size(); i++) {
                        MP3 mp3FromList = (MP3) MP3PlayerGUI.this.mp3ListModel.getElementAt(i);
                        if (mp3FromList.compareMP3s(mp3)) {
                            System.out.println("Эта песня уже добавлена в список");
                            count++;
                            break;
                        }
                    }
                    if (count==0) MP3PlayerGUI.this.mp3ListModel.addElement(mp3);
                }
            }
        }
    }

    class DeleteSongActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            int[]indexPlayList = plsList.getSelectedIndices();
            if (indexPlayList.length > 0) {
                ArrayList<MP3> mp3ListForRemove = new ArrayList<>();
                for (int i=0; i< indexPlayList.length; i++) {
                    MP3 mp3 = (MP3) mp3ListModel.getElementAt(indexPlayList[i]);
                    mp3ListForRemove.add(mp3);
                }

                for (MP3 mp3: mp3ListForRemove) {
                    mp3ListModel.removeElement(mp3);
                }
            }
        }
    }

    class OpenPlsActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            FileUtils.addFileFilter(fc, playListFileFilter);
            int result = fc.showOpenDialog(MP3PlayerGUI.this);

            if (result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fc.getSelectedFile();
                DefaultListModel mp3ListModel = (DefaultListModel) FileUtils.deserialize(selectedFile.getPath());
                MP3PlayerGUI.this.mp3ListModel = mp3ListModel;
                plsList.setModel(mp3ListModel);
            }
        }
    }

    class PlaySongActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            playFile();
        }
    }

    class StopSongActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            player.stop();
        }
    }

    class PauseSongActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            player.pause();
        }
    }

    class SaveActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            FileUtils.addFileFilter(fc, playListFileFilter);
            int result = fc.showSaveDialog(MP3PlayerGUI.this);
            if (result==JFileChooser.APPROVE_OPTION) {
                File selectedFile = fc.getSelectedFile();
                if (selectedFile.exists()) {
                    int resultOverride = JOptionPane.showConfirmDialog(MP3PlayerGUI.this, "Файл существует", "Перезаписать?", JOptionPane.YES_NO_CANCEL_OPTION);
                    switch (resultOverride){
                        case JOptionPane.NO_OPTION:
                            actionPerformed(e);
                            return;
                        case JOptionPane.CANCEL_OPTION:
                            fc.cancelSelection();
                            return;

                    }
                    fc.approveSelection();
                }

                String fileExtension = FileUtils.getFileExtension(selectedFile);

                String fileNameForSave = (fileExtension != null&&fileExtension.equals(PLAYLIST_FILE_EXTENSION)) ? selectedFile.getPath() : selectedFile.getPath() + "." + PLAYLIST_FILE_EXTENSION;

                FileUtils.serialize(mp3ListModel, fileNameForSave);
            }
        }
    }

    class SearchActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String searchStr = searchSongTextField.getText();

            if (searchStr ==null || searchStr.trim().equals(EMPTY_STRING)) {
                return;
            }

            ArrayList<Integer> mp3FindedIndexes = new ArrayList<>();

            for (int i=0; i<mp3ListModel.size(); i++) {
                MP3 mp3 = (MP3) mp3ListModel.getElementAt(i);

                if (mp3.getName().toUpperCase().contains(searchStr.toUpperCase())) {
                    mp3FindedIndexes.add(i);
                }
            }

            int[] selectedIndexes = new int [mp3FindedIndexes.size()];

            if (selectedIndexes.length ==0) {
                JOptionPane.showMessageDialog(MP3PlayerGUI.this, "Поиск по строке \'" + searchStr + "\' не дал результатов");
                searchSongTextField.requestFocus();
                searchSongTextField.selectAll();
                return;
            }

            for (int i=0; i<selectedIndexes.length; i++) {
                selectedIndexes[i] = mp3FindedIndexes.get(i).intValue();
            }

            plsList.setSelectedIndices(selectedIndexes);
        }
    }

    public MP3PlayerGUI() {

        menuBar.add(fileMenu);
        menuBar.add(serviceMenu);
        fileMenu.add(openItem);
        fileMenu.add(saveItem);
        fileMenu.add(new JSeparator());
        fileMenu.add(exitItem);
        serviceMenu.add(changeSkinMenu);
        changeSkinMenu.add(menuSkin1);
        changeSkinMenu.add(menuSkin2);

        fc.setAcceptAllFileFilterUsed(false);
        fc.setMultiSelectionEnabled(true);
        plsList.setModel(mp3ListModel);
        jpm.add(addSongMenuItem);
        jpm.add(deleteSongMenuItem);
        jpm.add(new JSeparator());
        jpm.add(openPlsMenuItem);
        jpm.add(clearPlsMenuItem);
        jpm.add(new JSeparator());
        jpm.add(playSongMenuItem);
        jpm.add(stopSongMenuItem);
        jpm.add(pauseSongMenuItem);

        addSongMenuItem.setIcon(new ImageIcon(getClass().getResource("/images/plus_16.png")));
        deleteSongMenuItem.setIcon(new ImageIcon(getClass().getResource("/images/remove_icon.png")));
        openPlsMenuItem.setIcon(new ImageIcon(getClass().getResource("/images/open-icon.png")));
        clearPlsMenuItem.setIcon(new ImageIcon(getClass().getResource("/images/clear.png")));
        playSongMenuItem.setIcon(new ImageIcon(getClass().getResource("/images/Play.png")));
        stopSongMenuItem.setIcon(new ImageIcon(getClass().getResource("/images/stop-red-icon.png")));
        pauseSongMenuItem.setIcon(new ImageIcon(getClass().getResource("/images/Pause-icon.png")));

        openItem.setIcon(new ImageIcon(getClass().getResource("/images/open-icon.png")));
        saveItem.setIcon(new ImageIcon(getClass().getResource("/images/save_16.png")));
        exitItem.setIcon(new ImageIcon(getClass().getResource("/images/exit.png")));
        changeSkinMenu.setIcon(new ImageIcon(getClass().getResource("/images/gear_16.png")));

        openItem.addActionListener(new OpenPlsActionListener());

        addButton.addActionListener(new AddSongActionListener());

        removeButton.addActionListener(new DeleteSongActionListener());

        nextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectNextSong();
            }
        });

        prevButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectPrevSong();
            }
        });

        nextSongButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectNextSong();
            }
        });

        prevSongButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectPrevSong();
            }
        });

        playButton.addActionListener(new PlaySongActionListener());

        stopButton.addActionListener(new StopSongActionListener());

        resumeButton.addActionListener(new PauseSongActionListener());

        saveItem.addActionListener(new SaveActionListener());

        searchButton.addActionListener(new SearchActionListener());

        plsList.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    jpm.show(e.getComponent(), e.getX(), e.getY());
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    jpm.show(e.getComponent(), e.getX(), e.getY());
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getModifiers()== InputEvent.BUTTON1_MASK&&e.getClickCount()==2) {
                    playFile();
                }
            }
        });

        addSongMenuItem.addActionListener(new AddSongActionListener());
        deleteSongMenuItem.addActionListener(new DeleteSongActionListener());
        openPlsMenuItem.addActionListener(new OpenPlsActionListener());

        clearPlsMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mp3ListModel.clear();
            }
        });

        playSongMenuItem.addActionListener(new PlaySongActionListener());
        stopSongMenuItem.addActionListener(new StopSongActionListener());
        pauseSongMenuItem.addActionListener(new PauseSongActionListener());

        menuSkin1.addActionListener(new ChangeSkinActionListener(MP3PlayerGUI.this, new MetalLookAndFeel()));
        menuSkin2.addActionListener(new ChangeSkinActionListener(MP3PlayerGUI.this, new NimbusLookAndFeel()));

        volumeSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                player.setVolume(volumeSlider.getValue(), volumeSlider.getMaximum());

                if (volumeSlider.getValue()==0) {
                    btnMute.setSelected(true);
                } else btnMute.setSelected(false);
            }
        });

        btnMute.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (btnMute.isSelected()) {
                    currentVolumeValue = volumeSlider.getValue();
                    volumeSlider.setValue(0);
                } else volumeSlider.setValue(currentVolumeValue);
            }
        });

        plsList.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode()==VK_ENTER) {
                    playFile();
                };
            }
        });

        searchSongTextField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String searchStr = searchSongTextField.getText();

                if (searchStr ==null || searchStr.trim().equals(EMPTY_STRING)) {
                    return;
                }

                ArrayList<Integer> mp3FindedIndexes = new ArrayList<>();

                for (int i=0; i<mp3ListModel.size(); i++) {
                    MP3 mp3 = (MP3) mp3ListModel.getElementAt(i);

                    if (mp3.getName().toUpperCase().contains(searchStr.toUpperCase())) {
                        mp3FindedIndexes.add(i);
                    }
                }

                int[] selectedIndexes = new int [mp3FindedIndexes.size()];

                if (selectedIndexes.length ==0) {
                    JOptionPane.showMessageDialog(MP3PlayerGUI.this, "Поиск по строке \'" + searchStr + "\' не дал результатов");
                    searchSongTextField.requestFocus();
                    searchSongTextField.selectAll();
                    return;
                }

                for (int i=0; i<selectedIndexes.length; i++) {
                    selectedIndexes[i] = mp3FindedIndexes.get(i).intValue();
                }

                plsList.setSelectedIndices(selectedIndexes);
                plsList.requestFocus();
            }
        });

        progressSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (progressSlider.getValueIsAdjusting()==false) {
                    if (moveAutomatic ==true) {
                        moveAutomatic = false;
                        posValue = progressSlider.getValue() * 1.0 / 1000;
                        processSeek(posValue);
                    }
                } else {
                    moveAutomatic = true;
                    movingFromJump =true;
                }
            }
        });

        setMinimumSize(new Dimension(300,380));

        setJMenuBar(menuBar);
        add(mainPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);

    }


    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                new MP3PlayerGUI();
            }
        });
    }
}
